gff2ps (0.98l-6) unstable; urgency=medium

  * Fix Homepage
    Closes: #999490

 -- Andreas Tille <tille@debian.org>  Fri, 12 Nov 2021 08:32:28 +0100

gff2ps (0.98l-5) unstable; urgency=medium

  * Fix watch file
  * Standards-Version: 4.6.0 (routine-update)
  * Apply multi-arch hints.
    + gff2ps: Add Multi-Arch: foreign.

 -- Andreas Tille <tille@debian.org>  Thu, 30 Sep 2021 13:41:56 +0200

gff2ps (0.98l-4) unstable; urgency=medium

  * Team upload.
  * Remove Nelson A. de Oliveira from uploaders (Closes: #962107)
  * Update to debhelper compat level 13
  * Bump Standards-Version to 4.5.1

 -- tony mancill <tmancill@debian.org>  Thu, 19 Nov 2020 19:36:49 -0800

gff2ps (0.98l-3) unstable; urgency=medium

  * Fix homepage (thanks for the hint to Ben Tris)
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Refer to specific version of license GPL-2+.

 -- Andreas Tille <tille@debian.org>  Fri, 17 Apr 2020 11:13:53 +0200

gff2ps (0.98l-2) unstable; urgency=medium

  * Team upload.
  * Add patch to fix exit status
  * Add autopkgtest
  * Standards-Version: 4.1.5

 -- Liubov Chuprikova <chuprikovalv@gmail.com>  Wed, 11 Jul 2018 13:17:27 +0300

gff2ps (0.98l-1) unstable; urgency=medium

  * Upstream does not provide a tarball but rather the gziped source file
    thus a get-orig-source script is provided to create the needed
    tarball automatically
  * Standards-Version: 4.1.4
  * Point Vcs-fields to Salsa
  * debhelper 11

 -- Andreas Tille <tille@debian.org>  Thu, 26 Apr 2018 16:04:02 +0200

gff2ps (0.98d-6) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * debhelper 10
  * Standards-Version: 4.1.1

 -- Andreas Tille <tille@debian.org>  Tue, 14 Nov 2017 16:15:06 +0100

gff2ps (0.98d-5) unstable; urgency=medium

  * Moved debian/upstream to debian/upstream/metadata
  * cme fix dpkg-control
  * debhelper 9

 -- Andreas Tille <tille@debian.org>  Mon, 11 Jan 2016 15:37:47 +0100

gff2ps (0.98d-4) unstable; urgency=low

  [ Nelson A. de Oliveira ]
  * Remove linda override;

  [ Andreas Tille ]
  * debian/upstream: add citation
  * debian/control:
     - Standards-Version: 3.9.3 (no changes needed)
     - Fixed Vcs-Svn
  * debian/rules: switch from cdbs to dh
  * debian/source/format: 3.0 (quilt)
  * debian/copyright: DEP5
  * debian/gff2ps.1: Fixed minor lintian issue

 -- Andreas Tille <tille@debian.org>  Thu, 03 May 2012 11:35:44 +0200

gff2ps (0.98d-3) unstable; urgency=low

  * Updated Standards-Version;
  * Changed Maintainer to the Debian-Med Packaging Team;
  * Added new Homepage field;
  * Added Vcs-* fields.

 -- Nelson A. de Oliveira <naoliv@debian.org>  Sun, 24 Feb 2008 00:18:36 -0300

gff2ps (0.98d-2) unstable; urgency=low

  * Updated debian/copyright file;
  * Bumped Standards-Version to 3.7.2 (no changes needed);
  * Moved cdbs and debhelper to Build-Depends;
  * Using debhelper compatibility level 5;
  * New debian/linda.override:
    - Updated debian/rules to install the override.
  * Removed "ghostview" from Recommends;
  * Added a space before Homepage: on the package long description.

 -- Nelson A. de Oliveira <naoliv@gmail.com>  Thu, 27 Jul 2006 02:28:37 -0300

gff2ps (0.98d-1) unstable; urgency=low

  * Initial Release (Closes: #310518).

 -- Nelson A. de Oliveira <naoliv@gmail.com>  Tue, 22 Mar 2005 12:22:50 -0300
