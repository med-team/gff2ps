Source: gff2ps
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/med-team/gff2ps
Vcs-Git: https://salsa.debian.org/med-team/gff2ps.git
Homepage: https://genome.crg.es/software/gff2ps/
Rules-Requires-Root: no

Package: gff2ps
Architecture: all
Depends: gawk,
         ${misc:Depends}
Recommends: gv | postscript-viewer
Multi-Arch: foreign
Description: produces PostScript graphical output from GFF-files
 gff2ps is a script program developed with the aim of converting gff-formatted
 records into high quality one-dimensional plots in PostScript. Such plots
 maybe useful for comparing genomic structures and to visualizing outputs from
 genome annotation programs.
 It can be used in a very simple way, because it assumes that the GFF file
 itself carries enough formatting information, but it also allows through a
 number of options and/or a configuration file, for a great degree of
 customization.
